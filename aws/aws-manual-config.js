
const awsmanual = {
  Auth: {
    identityPoolId: "us-east-1:cf3657e4-1759-4c7c-a362-ff90f30e3744", //REQUIRED - Amazon Cognito Identity Pool ID
    region: "us-east-1", // REQUIRED - Amazon Cognito Region
    userPoolId: "us-east-1_7xhbrVx0p", //OPTIONAL - Amazon Cognito User Pool ID
    userPoolWebClientId: "7rdmeeruu75hkm1s9vlu7nvbbe" //OPTIONAL - Amazon Cognito Web Client ID
  },
  Storage: {
    AWSS3: {
      bucket: "todo14d85965aeb8448c93c623eec2e881d9201913-dev", //REQUIRED -  Amazon S3 bucket
      region: "us-east-1" //OPTIONAL -  Amazon service region
    }
  }
};

export default awsmanual;
